﻿using System.Collections.Generic;

namespace Common
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Phone> Phones { get; set; } = new List<Phone>();
        public override string ToString()
        {
            return $"{Id}. {Name}";
        }

    }
}
