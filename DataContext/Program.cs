﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataContext
{
    class Program
    {
        static void Main(string[] args)
        {
            using DataModel db = new();
            ShowQueryable(db);
            ShowEnumerable(db);
            Sorting(db);
            Join(db);
            Grouping(db);
            Union(db);
            Intersect(db);
            Except(db);
            Console.ReadKey();
        }
        static void Except(DataModel db)
        {
            var q = (from p in db.Phones where p.Price > 10000 select p)
            .Except(from p in db.Phones where p.Name.Contains("Samsung") select p);
            foreach(var i in q) Console.WriteLine(i);
        }
        static void Intersect(DataModel db)
        {
            var q = (from p in db.Phones where p.Price < 25000 select p)
            .Intersect(from p in db.Phones where p.Name.Contains("Xiaomi") select p);
            foreach(var i in q) Console.WriteLine(i);
        }
        static void Union(DataModel db)
        {
           var q = (from p in db.Phones where p.Price > 15000 select p)
           .Union(from p in db.Phones where p.Name.Contains("LG") select p);
           foreach(var i in q) Console.WriteLine(i);
        }
        static void Grouping(DataModel db)
        {
            var q = from p in db.Phones group p by p.Company.Name;
            foreach (var g in q)
            {
                Console.WriteLine($"\tGroup by [{g.Key}] contains {g.Count()} phones");
                foreach(var i in g) Console.WriteLine(i);
            }
        }
        static void Join(DataModel db)
        {
            var q  = from p in db.Phones
                     join c in db.Companies
                     on p.CompanyId equals c.Id
                     select new {p.Id, p.Name, p.Price, Company = c.Name};
            foreach(var i in q) Console.WriteLine($"{i.Id}. {i.Name} ${i.Price} by {i.Company} Inc.");
        }
        static void Sorting(DataModel db)
        {
            var q = from p in db.Phones
                    orderby p.Name
                    select p;
            Console.WriteLine("----- order by name -----------");
            foreach(var i in q) Console.WriteLine(i);
            q = from p in db.Phones orderby p.Price descending select p;
            Console.WriteLine("----- order by price descending -----------");
            foreach(var i in q) Console.WriteLine(i);
            q = from p in db.Phones
                orderby p.Price descending, p.Name 
                select p;
            Console.WriteLine("----- order by price descending and by name -----------");
            foreach(var i in q) Console.WriteLine(i);
        }
        static void ShowEnumerable(DataModel db)
        {
            var q = from p in db.Phones.ToList() where p.Id < 4 select p;
            Console.WriteLine(q);
            foreach(var i in q) Console.WriteLine(i);
        }
        static void ShowQueryable(DataModel db)
        {
            var q = from p in db.Phones where p.Id > 3 select p;
            Console.WriteLine(q);
            foreach(var i in q) Console.WriteLine(i);
        }
    }
}
