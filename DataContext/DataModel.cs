﻿using System;
using System.Data.Entity;
using System.Linq;
using Common;

namespace DataContext
{
    public class DataModel : DbContext
    {
        static DataModel()
        {
            Database.SetInitializer(new ContextInitializer());
        }
        public DataModel()
            : base("name=DataModel")
        {
        }
        public virtual DbSet<Phone> Phones { get; set; }
        public virtual DbSet<Company> Companies { get; set; }

    }
}