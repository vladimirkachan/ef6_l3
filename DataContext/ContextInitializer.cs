﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DataContext
{
    class ContextInitializer : DropCreateDatabaseAlways<DataModel>
    {
        protected override void Seed(DataModel context)
        {
            Company c1 = new Company { Name = "Samsung" };
            Company c2 = new Company { Name = "Microsoft" };
            Company c3 = new() {Name = "LG"}, c4 = new() {Name = "Xiaomi"};

            context.Companies.Add(c1);
            context.Companies.Add(c2);
            context.Companies.AddRange(new[]{c3, c4});
            context.SaveChanges();

            Phone p1 = new Phone { Name = "Samsung Galaxy S5", Price = 20000, Company = c1 };
            Phone p2 = new Phone { Name = "Samsung Galaxy S4", Price = 15000, Company = c1 };
            Phone p3 = new Phone { Name = "Nokia Lumia 930", Price = 10000, Company = c2 };
            Phone p4 = new Phone { Name = "Nokia Lumia 830", Price = 8000, Company = c2 };
            Phone p5 = new() {Name = "LG Nexus 5", Price = 8000, Company = c3},
                  p6 = new() {Name = "Xiaomi Redmi Note 10", Price = 20000, Company = c4},
                  p7 = new() {Name = "Xiaomi Redmi Note 10 Pro", Price = 25000, Company = c4};

            context.Phones.AddRange(new [] { p1, p2, p3, p4, p5, p6, p7 });
            context.SaveChanges();
        }
    }
}
