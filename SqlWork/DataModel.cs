﻿using System;
using System.Data.Entity;
using System.Linq;
using Common;

namespace SqlWork
{
    public class DataModel : DbContext
    {
        public DataModel() : base("name=SqlDataModel") {}

        public virtual DbSet<Phone> Phones { get; set; }
        public virtual DbSet<Company> Companies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>().HasMany(c => c.Phones).WithRequired(p => p.Company)
                        .HasForeignKey(p => p.CompanyId);
        }
    }

}