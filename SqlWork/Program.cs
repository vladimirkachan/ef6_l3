﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace SqlWork
{
    class Program
    {
        static void Main(string[] args)
        {
            using DataModel db = new();
            Console.WriteLine(db.Database.Connection.ConnectionString);
            Console.WriteLine("-------- 1 ---------");
            var companyQuery = db.Database.SqlQuery<Company>("SELECT * FROM Companies");
            foreach(var c in companyQuery) Console.WriteLine(c);
            Console.WriteLine("-------- 2 ---------");
            SqlParameter parameter = new("@name", "%Samsung%");
            var phoneQuery = db.Database.SqlQuery<Phone>("SELECT * FROM Phones WHERE Name LIKE (@name)", parameter);
            foreach(var p in phoneQuery) Console.WriteLine(p);
            Console.WriteLine("-------- 3 ---------");
            Console.WriteLine($"\tInserted = {db.Database.ExecuteSqlCommand("INSERT Companies (Name) VALUES ('HTC')")}");
            companyQuery = db.Database.SqlQuery<Company>("SELECT * FROM Companies");
            foreach(var c in companyQuery) Console.WriteLine(c);
            Console.WriteLine("-------- 4 ---------");
            Console.WriteLine($"\tUpdated = {db.Database.ExecuteSqlCommand("UPDATE Companies SET [Name]='Fly' WHERE [Name]='HTC'")}");
            companyQuery = db.Database.SqlQuery<Company>("SELECT * FROM Companies");
            foreach(var c in companyQuery) Console.WriteLine(c);
            Console.WriteLine("-------- 5 ---------");
            Console.WriteLine($"\tDeleted = {db.Database.ExecuteSqlCommand("DELETE FROM Companies WHERE [Name]='Fly'")}");
            companyQuery = db.Database.SqlQuery<Company>("SELECT * FROM Companies");
            foreach(var c in companyQuery) Console.WriteLine(c);
            Console.WriteLine("-------- 6 ---------");
            Console.WriteLine("\t SQL Function");
            phoneQuery = db.Database.SqlQuery<Phone>("SELECT * FROM GetPhonesLessThanPrice (@price)", new SqlParameter("@price", 9000));
            foreach(var p in phoneQuery) Console.WriteLine(p);
            Console.WriteLine("-------- 7 ---------");
            Console.WriteLine("\t SQL Stored procedure");
            phoneQuery = db.Database.SqlQuery<Phone>("GetPhonesByCompany @name", new SqlParameter("@name", "Samsung"));
            foreach(var p in phoneQuery) Console.WriteLine(p);
            Console.WriteLine("-------- 8 ---------");
            Console.ReadKey();
        }
    }
}
